from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.forms import inlineformset_factory
from .models import *
from .forms import CustomerForm,OrderForm,CreateUserForm
from .filter import Orderfilter
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.decorators import login_required
from .decorators import unauthenticated_user,allowed_users, admin_only
# Create your views here.

@unauthenticated_user
def loginpage(request):
    if request.method=='POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request,username=username,password=password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else:
            messages.info(request,'Username or password is incorrect')

    return render(request,'accounts/login.html')
def logoutpage(request):
    logout(request)
    return redirect('login')
def users(request):
    return render(request,'accounts/users.html')

@unauthenticated_user
def register(request):
    form = CreateUserForm()

    if request.method=='POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request,'Account was created for '+user)
            return redirect('login')

    return render(request,'accounts/register.html',{'form':form})

@login_required(login_url='login')
@admin_only
def home(request):
    customers = Customer.objects.all()
    orders = Order.objects.all()
    total_order = orders.count()
    total_delivered = orders.filter(status='Delivered').count()
    total_pending = orders.filter(status='Pending').count()

    return render(request,'accounts/dashboard.html',{'customers':customers,'orders':orders,'total_delivered':total_delivered,'total_pending':total_pending,'total_order':total_order})
@login_required(login_url='login')
def products(request):
    products= Product.objects.all()
    return render(request,'accounts/products.html',{'products':products})

@login_required(login_url='login')
def customers(request,pk):
    customer = Customer.objects.get(id=pk)
    orders = customer.order_set.all()
    myFilter = Orderfilter(request.GET,queryset=orders)
    orders = myFilter.qs
    total_order = orders.count()
    return render(request,'accounts/customers.html',{'customer':customer,'orders':orders,'total_order':total_order,'myFilter':myFilter})

@login_required(login_url='login')
def newcustomer(request): #insert
    form = CustomerForm()
    if request.method=='POST':
        form = CustomerForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/')
        
    return render(request,'accounts/form.html',{'form':form})

@login_required(login_url='login')
def neworder(request,pk): #insert
    #OrderFormSet = inlineformset_factory(Customer,Order,fields={'product','status'})
    customer = Customer.objects.get(id=pk)
    #formset= OrderFormSet(instance=customer)
    formset = OrderForm(initial={'customer':customer})
    if request.method=='POST':
        form = OrderForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/')
        
    return render(request,'accounts/orderform.html',{'formset':formset})

@login_required(login_url='login')
def new_order(request):  # insert
    formset = OrderForm()
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid:
            form.save()
            return redirect('/')

    return render(request, 'accounts/orderform.html', {'formset': formset})

@login_required(login_url='login')
def updateorder(request,pk):  #update
    update= Order.objects.get(id=pk)

    if request.method=='POST':
        form = OrderForm(request.POST,instance=update)
        if form.is_valid:
            form.save()
            return redirect('/')

    form = OrderForm(instance=update)
    return render(request,'accounts/form.html',{'form':form})

@login_required(login_url='login')
def deleteorder(request,pk): #delete
    dlt= Order.objects.get(id=pk)
    dlt.delete()
    return redirect('/')
