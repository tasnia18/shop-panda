from django.urls import path,include
from . import views

urlpatterns = [
    path('', views.home,name= 'home'),
    path('customers/<str:pk>', views.customers,name='customer'),
    path('products/', views.products,name='products'),

    path('login/', views.loginpage,name='login'),
    path('logout/', views.logoutpage,name='logout'),
    path('register/', views.register,name='register'),

    path('users/', views.users,name='users'),

    path('newcustomer/', views.newcustomer,name='newcustomer'),
    path('neworder/<str:pk>', views.neworder,name='neworder'),
    path('new_order/', views.new_order,name='new_order'),
    path('updateorder/<str:pk>', views.updateorder,name='updateorder'),
    path('deleteorder/<str:pk>', views.deleteorder,name='deleteorder'),
    


]
